import { Component, ViewChildren, OnInit, AfterViewInit, ChangeDetectorRef, QueryList } from '@angular/core';

import { TodoComponent } from './components/todo/todo.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})  
export class AppComponent implements OnInit, AfterViewInit {
  
  todos: string[] = [];
  @ViewChildren(TodoComponent) todoViewChildren: QueryList<TodoComponent>;
  
  constructor(private _changeDetectorRef: ChangeDetectorRef) {}
  
  ngOnInit() {
    this.todos = this.fillTodos();
  }
  
  ngAfterViewInit() {
    this.todoViewChildren.forEach( (item) => { item.todo = 'Nothing to do'; });
    this._changeDetectorRef.detectChanges();
  }
    
  fillTodos(): string[] {
    return [
      'Go work',
      'Walk the dog',
      'do homework'
    ];
  }
  
}
